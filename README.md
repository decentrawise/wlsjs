# WLSJS
WLSJS the JavaScript API for WhaleShares blockchain

# Documentation

- [Install](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#install)
- [Browser](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#browser)
- [Config](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#config)
- [Database API](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#api)
    - [Subscriptions](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#subscriptions)
    - [Tags](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#tags)
    - [Blocks and transactions](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#blocks-and-transactions)
    - [Globals](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#globals)
    - [Keys](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#keys)
    - [Accounts](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#accounts)
    - [Market](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#market)
    - [Authority / validation](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#authority--validation)
    - [Votes](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#votes)
    - [Content](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#content)
    - [Witnesses](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#witnesses)
- [Login API](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#login)
- [Follow API](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#follow-api)
- [Broadcast API](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#broadcast-api)
- [Broadcast](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#broadcast)
- [Auth](https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc#auth)


Here is full documentation:
https://gitlab.com/beyondbitcoin/wlsjs/tree/legacy/doc

## Browser 
```html 
<script src="./wlsjs.min.js"></script>
<script>
wlsjs.api.getAccounts(['officialfuzzy', 'powerpics'], function(err, response){
    console.log(err, response);
});
</script>
```

## Server
## Install
```
$ npm install @whaleshares/wlsjs --save
```

## WebSockets
wss://whaleshares.io/ws By Default<br/>
wss://beta.whaleshares.net/wss<br/>

## Examples
### Broadcast Vote
```js
var wlsjs = require('@whaleshares/wlsjs');

var wif = wlsjs.auth.toWif(username, password, 'posting');
wlsjs.broadcast.vote(wif, voter, author, permlink, weight, function(err, result) {
	console.log(err, result);
});
```

### Get Accounts
```js
wlsjs.api.getAccounts(['officialfuzzy', 'powerpics'], function(err, result) {
	console.log(err, result);
});
```

### Get State
```js 
wlsjs.api.getState('/trends/funny', function(err, result) {
	console.log(err, result);
});
```

## Contributions
Patches are welcome! Contributors are listed in the package.json file. Please run the tests before opening a pull request and make sure that you are passing all of them. If you would like to contribute, but don't know what to work on, check the issues list.

## Issues
When you find issues, please report them!

## License
MIT
